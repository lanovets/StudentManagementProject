package com.ivanovich.spring.service;

import com.ivanovich.spring.entity.CourseTaken;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {
    private List<CourseTaken> courses;

    public StudentService() {
        courses = new ArrayList<>();
    }

    public void addCourse(
            @Validated CourseTaken courseTaken
    ) {
        if (!courses.contains(courseTaken)) {
            courses.add(courseTaken);
        }
    }
}
