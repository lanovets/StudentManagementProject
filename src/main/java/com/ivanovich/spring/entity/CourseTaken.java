package com.ivanovich.spring.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Course_Taken")
public class CourseTaken {

    @Id
    private Long id;
    @ManyToOne
    Student student;


    public Long getId() {
        return id;
    }
}
