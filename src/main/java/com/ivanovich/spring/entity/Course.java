package com.ivanovich.spring.entity;

import jakarta.persistence.*;


@Entity(name = "Course")
public class Course {
    @Id
    @SequenceGenerator(
            name = "course_sequence",
            sequenceName = "course_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "course_sequence"
    )
    @Column(
            name = "id",
            updatable = false
    )
    private Long id;
    @Column(
            name = "name",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String name;


    public Course(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Course() {
    }
}

