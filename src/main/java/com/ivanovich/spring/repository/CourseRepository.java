package com.ivanovich.spring.repository;

import com.ivanovich.spring.entity.Course;
import org.springframework.data.repository.CrudRepository;


public interface CourseRepository extends CrudRepository<Course,Long> {

}
