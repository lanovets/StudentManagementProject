package com.ivanovich.spring.repository;

import com.ivanovich.spring.entity.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Long> {

}
