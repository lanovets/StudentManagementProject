package com.ivanovich.spring.controller;

import com.ivanovich.spring.entity.Student;
import com.ivanovich.spring.repository.StudentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(
        path = "/api/students",
        produces = "application/json"
)
public class StudentController {
    private final StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    @PostMapping(consumes="application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Student postTaco(@RequestBody Student student) {
        return studentRepository.save(student);
    }
}
