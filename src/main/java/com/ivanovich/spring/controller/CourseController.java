package com.ivanovich.spring.controller;

import com.ivanovich.spring.entity.Course;
import com.ivanovich.spring.repository.CourseRepository;
import com.ivanovich.spring.service.CourseService;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping(
        path = "/api/courses",
        produces = "application/json"
)
public class CourseController {
    private final CourseRepository courseRepository;


    public CourseController(CourseService courseService, CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @GetMapping("/student/{studentId}/courses")
    public Iterable<Course> getAllCourses(
            @PathVariable("studentId") Long studentId
    ) {
//        PageRequest page = PageRequest.of(0, 12, Sort.by("createdAt").descending());
        return courseRepository.findAll();
    }

    @GetMapping("/student/{courseId}")
    public ResponseEntity<Course> courseById(
            @PathVariable("courseId") Long courseId
    ) {
        Optional<Course> optionalCourse = courseRepository.findById(courseId);
        return optionalCourse.
                map(course -> new ResponseEntity<>(course, HttpStatus.OK)).
                orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("student/{courseId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCourse(@PathVariable("courseId") Long orderId) {
        try {
            courseRepository.deleteById(orderId);
        } catch (EmptyResultDataAccessException ignored) {
        }
    }

}
